      $(function () {
        $("#reserve").on("show.bs.modal", function (e) {
          console.log("opening the modal window");
          $("#reserveBtn").prop("disabled", true);
          $("#reserveBtn").removeClass("btn-danger");
          $("#reserveBtn").addClass("btn-secondary");
        });
        $("#reserve").on("shown.bs.modal", function (e) {
          console.log("opened modal window");
        });
        $("#reserve").on("hide.bs.modal", function (e) {
          console.log("closing the modal window");
        });
        $("#reserve").on("hidden.bs.modal", function (e) {
          console.log("closed modal window");
          $("#reserveBtn").prop("disabled", false);
          $("#reserveBtn").toggleClass("btn-danger");
        });
        $("[data-bs-toggle='popover']").popover();
        $('.carousel').carousel({
          interval:2000
        });
    });