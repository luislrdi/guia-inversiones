"use strict";

function defaultTask(cb) {
  task("sass", function () {
    gulp
      .src("./css/*.scss")
      .pipe(sass().on("error", logError))
      .pipe(dest("./css"));
  });
  // place code for your default task here
  task("sass:watch", function () {
    watch("./css/*.scss", ["sass"]);
  });

  task("browser-sync", function () {
    var files = [
      "./*.html",
      "./css/*.css",
      "./img/*.{png, jpg, gif}",
      "./js/*.js",
    ];
    browserSync.init(files, {
      server: {
        baseDir: "./",
      },
    });
  });

  task("default", ["browser-sync"], function () {
    gulp.start("sass:watch");
  });

  task("clean", function () {
    return del(["dist"]);
  });

  task("copyfonts", function () {
    gulp
      .src(
        "./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*"
      )
      .pipe(gulp.dest("./dist/fonts"));
  });

  task("imagemin", function () {
    return gulp
      .src("./images/*.{png, jpg, jpeg, gif}")
      .pipe(
        imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })
      )
      .pipe(gulp.dest("dist/images"));
  });

  task("usemin", function () {
    return gulp.src("./*.html").pipe(
      flatmap(function (stream, file) {
        return stream
          .pipe(
            usemin({
              css: [rev()],
              html: [
                function () {
                  return htmlmin({ collapseWhitespace: true });
                },
              ],
              js: [uglify(), rev()],
              inlinejs: [uglify()],
              inlinecss: [cleanCss(), "concat"],
            })
          )
          .pipe(gulp.dest("dist/"));
      })
    );
  });

  task("build", ["clean"], function () {
    start("copyfonts", "imagemin", "usemin");
  });
  cb();

  function requireOrImport(path, callback) {
    var err = null;
    var cjs;
    try {
      cjs = require(path);
    } catch (e) {
      if (pathToFileURL && importESM && e.code === "ERR_REQUIRE_ESM") {
        // This is needed on Windows, because import() fails if providing a Windows file path.
        var url = pathToFileURL(path);
        importESM(url).then(function (esm) {
          callback(null, esm);
        }, callback);
        return;
      }
      err = e;
    }
    process.nextTick(function () {
      callback(err, cjs);
    });
  }
  requireOrImport(env.configPath, function (err, exported) {
    // Before import(), if require() failed we got an unhandled exception on the module level.
    // So console.error() & exit() were added here to mimic the old behavior as close as possible.
    if (err) {
      console.error(err);
      exit(1);
    }

    registerExports(gulpInst, exported);

    // Always unmute stdout after gulpfile is required
    stdout.unmute();

    var tree;
    if (opts.tasksSimple) {
      tree = gulpInst.tree();
      return logTasksSimple(tree.nodes);
    }
    if (opts.tasks) {
      tree = gulpInst.tree({ deep: true });
      if (config.description && typeof config.description === "string") {
        tree.label = config.description;
      } else {
        tree.label = "Tasks for " + ansi.magenta(tildify(env.configPath));
      }

      return logTasks(tree, opts, getTask(gulpInst));
    }
    if (opts.tasksJson) {
      tree = gulpInst.tree({ deep: true });
      if (config.description && typeof config.description === "string") {
        tree.label = config.description;
      } else {
        tree.label = "Tasks for " + tildify(env.configPath);
      }

      var output = JSON.stringify(copyTree(tree, opts));

      if (typeof opts.tasksJson === "boolean" && opts.tasksJson) {
        return console.log(output);
      }
      return fs.writeFileSync(opts.tasksJson, output, "utf-8");
    }
    try {
      log.info("Using gulpfile", ansi.magenta(tildify(env.configPath)));
      var runMethod = opts.series ? "series" : "parallel";
      gulpInst[runMethod](toRun)(function (err) {
        if (err) {
          exit(1);
        }
      });
    } catch (err) {
      log.error(ansi.red(err.message));
      log.error("To list available tasks, try running: gulp --tasks");
      exit(1);
    }
  });
}

// var browserSync = require("browser-sync");

// import { dest, watch, start, src as _src } from "gulp";
// import sass, { logError } from "gulp-sass";
// import del from "del";
// import imagemin from "gulp-imagemin";
// import uglify from "gulp-uglify";
// // import usemin from "gulp-usemin";
// import rev from "gulp-rev";
// import cleanCss from "gulp-clean-css";
// // import flatmap from "gulp-flatmap";
// import htmlmin from "gulp-htmlmin";
// // import { pathToFileURL } from "url";
// // var requireOrImport = require("../../shared/require-or-import");

exports.default = defaultTask;
